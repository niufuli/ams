package com.zealcomm;

public class Reason {

    private String reason;
    private String appointState;
    public String getReason() {
        return reason;
    }

    public String getAppointState() {
        return appointState;
    }

    public void setAppointState(String appointState) {
        this.appointState = appointState;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
