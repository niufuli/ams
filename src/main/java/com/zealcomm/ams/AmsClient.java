package com.zealcomm.ams;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.zealcomm.Reason;
import com.zealcomm.base.MessageData;
import com.zealcomm.base.TLSSocketFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Arrays;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.OkHttpClient;

public class AmsClient {

    private final String TAG = "AmsClient";
    private final String RING_EVENT = "ring";
    private final String AGENT_LOGIN = "agent-login";
    private final String ERROR_ENENT = "error";
    private final String OK_EVENT = "ok";
    private final String AGENT_LOGOUT = "agent-logout";
    private final String STOP_RING = "stop-ringing";
    private final String DROP = "drop";
    private final String REPLACED = "replaced";
    private final String AGENT_CHECK_IN = "agent-checkIn";
    private final String AGENT_CHECK_OUT = "agent-checkOut";

    private static AmsClient instance;
    private String mToken;
    private Socket mAmsSocket;
    private boolean isConnected;
    private final int MAX_RECONNECT_ATTEMPTS = 5;
    private static final long DURATION = 200L;
    private IO.Options options;
    private String mAmsServerUrl;
    private Context mContext;
    private String reconnectionTicket;
    private AmsEventMessage mAmsEventMessage;

    public static AmsClient getInstance() {
        if (instance == null) {
            instance = new AmsClient();
        }
        return instance;
    }

    private AmsClient() {
    }

    public void init(Context context, AmsEventMessage eventMessage) {
        mAmsEventMessage = eventMessage;
        mContext = context;
        options = new IO.Options();
        options.forceNew = true;
        options.reconnection = true;
        options.reconnectionAttempts = MAX_RECONNECT_ATTEMPTS;
        options.secure = true;
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        if (TLSSocketFactory.sslContext != null) {
            clientBuilder.sslSocketFactory(TLSSocketFactory.delegate);
        }
        if (TLSSocketFactory.mHostnameVerifier != null) {
            clientBuilder.hostnameVerifier(TLSSocketFactory.mHostnameVerifier);
        }
        OkHttpClient httpClient = clientBuilder.build();
        options.callFactory = httpClient;
        options.webSocketFactory = httpClient;
    }

    public void connect(String token, String url) {
        isConnected = false;
        mAmsServerUrl = url;
        mToken = token;
        Log.i(TAG, "connect ams");
        options.query = "token=" + mToken + "&servicePath=_ivcs_ams";
        if (mAmsSocket != null) {
            mAmsSocket.disconnect();
        }
        try {
            mAmsSocket = IO.socket(mAmsServerUrl, options);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        mAmsSocket.on(Socket.EVENT_CONNECT, onConnect);
        mAmsSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mAmsSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        // io.socket:socket.io-client 由 1.0.0 升级到 2.0.0 后将 Socket.EVENT_CONNECT_TIMEOUT 合并到 Socket.EVENT_CONNECT_ERROR 了
        // mAmsSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mAmsSocket.on(RING_EVENT, onRingEvent);
        mAmsSocket.on(STOP_RING, onStopRingEvent);
        mAmsSocket.on(DROP, onDropEvent);
        mAmsSocket.on(REPLACED, onReplacedEvent);
        mAmsSocket.connect();

    }

    public void login() {
        if (isConnected) {
            mAmsSocket.emit(AGENT_LOGIN, null, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "AGENT_LOGIN = " + Arrays.asList(args));
                    String status = args[0].toString();
                    if (status.equals(ERROR_ENENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AMS_LOGIN_FAILED);
                        messageData.setData(args[1].toString());
                        mAmsEventMessage.onAmsEvent(messageData);
                    } else if (status.equals(OK_EVENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AMS_LOGIN_SUCCESS);
                        messageData.setData(args[1]);
                        mAmsEventMessage.onAmsEvent(messageData);
                    }
                }
            });
        }
    }

    public void checkIn(JSONObject groupIds) {
        if (isConnected) {
            Log.i(TAG, "check in=" + groupIds.toString());
            mAmsSocket.emit(AGENT_CHECK_IN, groupIds, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "AGENT_CHECK_IN = " + Arrays.asList(args));
                    String status = args[0].toString();
                    if (status.equals(ERROR_ENENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AMS_CHECK_IN_FAILED);
                        Log.d(TAG, "AGENT_CHECK_IN failed = " + args[1].toString());
                        messageData.setData(args[1].toString());
                        mAmsEventMessage.onAmsEvent(messageData);
                    } else if (status.equals(OK_EVENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AMS_CHECK_IN_SUCCESS);
                        mAmsEventMessage.onAmsEvent(messageData);
                    }
                }
            });
        }
    }

    public void checkOut(JSONObject groupIds) {
        if (isConnected) {
            Log.i(TAG, "check out=" + groupIds.toString());
            mAmsSocket.emit(AGENT_CHECK_OUT, groupIds, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "AGENT_CHECK_OUT status = " + args[0].toString());
                    String status = args[0].toString();
                    if (status.equals(ERROR_ENENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AMS_CHECK_OUT_FAILED);
                        messageData.setData(args[1].toString());
                        mAmsEventMessage.onAmsEvent(messageData);
                    } else if (status.equals(OK_EVENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AMS_CHECK_OUT_SUCCESS);
                        mAmsEventMessage.onAmsEvent(messageData);
                    }
                }
            });
        }
    }

    public void hangup() {
        if (isConnected) {
            mAmsSocket.emit(AmsMessage.AGENT_HANGUP, null , new Ack() {
                @Override
                public void call(Object... args) {
                    String status = args[0].toString();
                    if (status.equals(ERROR_ENENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AGENT_HANGUP_FAILED);
                        messageData.setData(args[1].toString());
                        mAmsEventMessage.onAmsEvent(messageData);
                    } else if (status.equals(OK_EVENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AGENT_HANGUP_SUCCESS);
                        mAmsEventMessage.onAmsEvent(messageData);
                    }
                }
            });
        }
    }

    public void changeStatus(String agentStatus) {
        if (isConnected) {
            mAmsSocket.emit(agentStatus, null, new Ack() {
                @Override
                public void call(Object... args) {
                    String status = args[0].toString();
                    Log.i(TAG, agentStatus + " result =" + status);
                    MessageData messageData = new MessageData();
                    messageData.setType(agentStatus);
                    messageData.setData(status);
                    mAmsEventMessage.onAmsEvent(messageData);
                }
            });
        }
    }

    public void changeStatus(String agentStatus, Reason reason) {
        if (isConnected) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(new Gson().toJson(reason));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mAmsSocket.emit(agentStatus, jsonObject, new Ack() {
                @Override
                public void call(Object... args) {
                    String status = args[0].toString();
                    if (status.equals(OK_EVENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(agentStatus);
                        messageData.setData(OK_EVENT);
                        mAmsEventMessage.onAmsEvent(messageData);
                    } else {
                        MessageData messageData = new MessageData();
                        messageData.setType(agentStatus);
                        messageData.setData(args[1].toString());
                        mAmsEventMessage.onAmsEvent(messageData);
                    }

                }
            });
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (!isConnected) {
                isConnected = true;
                MessageData messageData = new MessageData();
                messageData.setType(AmsMessage.AMS_CONNECT_SUCCESS);
                mAmsEventMessage.onAmsEvent(messageData);
            }
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onDisconnect = " + args[0]);
            isConnected = false;
            MessageData messageData = new MessageData();
            messageData.setType(AmsMessage.AMS_DISCONNECT);
            mAmsEventMessage.onAmsEvent(messageData);
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onConnectError" + Arrays.asList(args));
            MessageData messageData = new MessageData();
            messageData.setType(AmsMessage.AMS_CONNECT_ERROR);
            mAmsEventMessage.onAmsEvent(messageData);
        }
    };

    private Emitter.Listener onRingEvent = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onRingEvent");
            if (isConnected) {
                JSONObject message = (JSONObject) args[0];
                Log.d(TAG, "onRingEvent message: " + message.toString());
                MessageData messageData = new MessageData();
                messageData.setType(AmsMessage.AMS_RING);
                messageData.setData(message);
                mAmsEventMessage.onAmsEvent(messageData);
                if (args.length > 1) {
                    Ack ack = (Ack) args[args.length - 1];
                    ack.call("ok");
                }
            } else {
                Log.e(TAG, "Error connecting");
            }
        }
    };
    private Emitter.Listener onStopRingEvent = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onStopRingEvent");
            if (isConnected) {
                MessageData messageData = new MessageData();
                messageData.setType(AmsMessage.AMS_STOP_RING);
                mAmsEventMessage.onAmsEvent(messageData);
            } else {
                Log.e(TAG, "Error connecting");
            }
        }
    };
    private Emitter.Listener onDropEvent = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onDropEvent");
            if (isConnected) {
                MessageData messageData = new MessageData();
                messageData.setType(AmsMessage.AMS_DROP);
                mAmsEventMessage.onAmsEvent(messageData);
            } else {
                Log.e(TAG, "Error connecting");
            }
        }
    };
    private Emitter.Listener onReplacedEvent = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onReplacedEvent");
            if (isConnected) {
                MessageData messageData = new MessageData();
                messageData.setType(AmsMessage.AMS_REPLACED);
                mAmsEventMessage.onAmsEvent(messageData);
            } else {
                Log.e(TAG, "Error connecting");
            }
        }
    };

    public void logout() {
        if (isConnected) {
            mAmsSocket.emit(AGENT_LOGOUT, null, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "AGENT_LOGIN status = " + args[0].toString());
                    String res = (String) args[0];
                    String status = args[0].toString();
                    if (status.equals(ERROR_ENENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AMS_LOGIN_OUT_FAILED);
                        messageData.setData(args[1].toString());
                        mAmsEventMessage.onAmsEvent(messageData);
                    } else if (status.equals(OK_EVENT)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(AmsMessage.AMS_LOGIN_OUT_SUCCESS);
                        mAmsEventMessage.onAmsEvent(messageData);
                    }
                }
            });
        }
    }

    public interface AmsEventMessage {
        void onAmsEvent(MessageData messageData);
    }

}
