package com.zealcomm.ams;

/**
 * ams client message type description.
 * */
public class AmsMessage {

    public static final String AMS_CONNECT_SUCCESS = "ams_connect_success";
    public static final String AMS_CONNECT_FAILED = "ams_connect_failed";

    public static final String AMS_LOGIN_SUCCESS = "ams_login_success";
    public static final String AMS_LOGIN_FAILED = "ams_login_failed";

    public static final String AMS_CHECK_IN_SUCCESS = "ams_check_in_success";
    public static final String AMS_CHECK_IN_FAILED = "ams_check_in_failed";
    public static final String AMS_CHECK_OUT_SUCCESS = "ams_check_out_success";
    public static final String AMS_CHECK_OUT_FAILED = "ams_check_out_failed";

    public static final String AMS_LOGIN_OUT_SUCCESS = "ams_login_out_success";
    public static final String AMS_LOGIN_OUT_FAILED = "ams_login_out_failed";

    public static final String AMS_DISCONNECT = "ams_disconnect";
    public static final String AMS_CONNECT_ERROR = "ams_connect_error";

    public static final String AMS_RING = "ams_ring";

    public static final String AMS_STOP_RING = "ams_stop_ring";

    public static final String AMS_DROP = "ams_drop";

    public static final String AMS_REPLACED = "ams_replaced";

    public static final String AGENT_READY = "agent-ready";
    public static final String AGENT_HANGUP = "agent-hangup";
    public static final String AGENT_HANGUP_SUCCESS = "agent-hangup-success";
    public static final String AGENT_HANGUP_FAILED = "agent-hangup-failed";
    public static final String AGENT_ANSWER = "agent-answer";
    public static final String AGENT_REJECT = "agent-reject";
    public static final String AGENT_UNREADY = "agent-unready";
    public static final String AGENT_APPOINT= "agent-appoint";
    public static final String AGENT_OTHERWORK = "agent-otherWork";

}
